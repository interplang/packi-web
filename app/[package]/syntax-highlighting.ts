export default function SyntaxHighlight(md: string): string {
    var split = md.split(new RegExp("<pre><code>|</code></pre>"));
    for (var i = 1; i < split.length; i += 2) {
        var code = split[i];
        var code = code.replaceAll(new RegExp("\\b(var|if|else|and|or|while|brk|cnt|fun|ref|ret|imp|do|in|is|struct|enum|atom|mod|use|not)\\b", "g"), "<span class=\"lang_keyword\">$1</span>");
        split[i] = "<pre><code>" + code + "</code></pre>";
    }
    for (var i = 0; i < split.length; i += 2) {
        split[i] = split[i].replaceAll(new RegExp("<h1>(.+)</h1>", "g"), "<h1 id=\"$1\">$1</h1>");
        split[i] = split[i].replaceAll(new RegExp("<h2>(.+)</h2>", "g"), "<h2 id=\"$1\">$1</h2>");
        split[i] = split[i].replaceAll(new RegExp("<h3>(.+)</h3>", "g"), "<h3 id=\"$1\">$1</h3>");
        split[i] = split[i].replaceAll("--brk--", "<br/>");
    }
    return split.join("");
}