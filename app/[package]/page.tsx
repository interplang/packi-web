
import axios from "axios";
import { Metadata } from "next";
import Link from 'next/link';
import { remark } from 'remark';
import html from 'remark-html';
import SyntaxHighlight from "./syntax-highlighting";
import Tabs from './tabs';

export async function generateMetadata({ params }: { params: { package: string } }): Promise<Metadata> {
  return {
    title: "Packi - " + params.package,
    description: "Packi package: " + params.package
  }
}

export default async function Page({ params }: { params: { package: string } }) {

  try {
    const summary: string = (await axios("http://" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY + ":" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY_PORT + "/packages/" + params.package + "/summary.txt")).data
    const latest_version: string = (await axios("http://" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY + ":" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY_PORT + "/packages/" + params.package + "/latest.txt")).data.trim()
    const files: string = "http://" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY + ":" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY_PORT + "/packages/" + params.package


    var readme = null;
    try {
      readme = (await axios("http://" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY + ":" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY_PORT + "/packages/" + params.package + "/readme.md")).data.trim()
    } catch (error) {
    }

    var readme_link = null;
    try {
      readme_link = (await axios("http://" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY + ":" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY_PORT + "/packages/" + params.package + "/readme.txt")).data.trim()
    } catch (error) {
    }

    var repo = null;
    try {
      repo = (await axios("http://" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY + ":" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY_PORT + "/packages/" + params.package + "/repo.txt")).data.trim()
    } catch (error) {
    }
  
    var doc = null;
    try {
      doc = (await axios("http://" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY + ":" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY_PORT + "/packages/" + params.package + "/doc.txt")).data.trim()
    } catch (error) {
    }

    if (readme_link != null) {
      readme = (await axios(readme_link)).data
    }

  
    const processedContent = await remark()
      .use(html)
      .process(readme.replace("  \n", "<br>"))
    var contentHtml = SyntaxHighlight(processedContent.toString())
    contentHtml = contentHtml.replaceAll("<h1", "<br><br><h1").replaceAll("<h2", "<br><br><h2")

    return (
      <div>
        <p style={{textAlign: "center", paddingTop: "50px"}}>
          <Link href="/">&lt; Back</Link>
        </p>
        <h1 style={{textAlign: "center", paddingTop: "25px"}}>{params.package}</h1>
        <p style={{textAlign: "center"}}>
          {latest_version}
        </p>
        <br/>
        <p style={{textAlign: "center"}}>
          {summary}
        </p>
        <br/>
        <Tabs readme={contentHtml} package={params.package} latest_version={latest_version} files={files} repo={repo} doc={doc}/>
      </div>
    )

  } catch {
    return (
      <div className="flex justify-center items-center h-full">
        <div>
          <div style={{fontSize: "4rem"}}>404</div>
          <div style={{fontSize: "2rem"}}>Package does not exist..</div>
          <Link href="/">Back to the homepage</Link>
        </div>
      </div>
    );
  }
}