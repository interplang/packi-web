"use client";

import { useSearchParams, useRouter } from "next/navigation";
import { useState } from "react";
import Link from 'next/link';

export default function PackageSearcher(params: {readme: string, package: string, latest_version: string, files: string, repo: string | null, doc: string | null}) {

  const router = useRouter();

  //const tab = useSearchParams().get("page");
  const [tab, setTab] = useState("readme");

  return (
    <div className="w-full">
      {(() => {
        switch(tab) {
          case "install":
            return (
              <div>
                <div className="flex justify-center">
                  <div className="tabs">
                    <a className="tab tab-bordered tab-active">Install</a> 
                    <a onClick={() => setTab("readme")} className="tab tab-bordered">Readme</a> 
                    <a onClick={() => setTab("links")} className="tab tab-bordered">Links</a>
                  </div>
                </div>
                <br/>
                <p style={{textAlign: "center"}}>
                  As a dependency:
                </p>
                <div style={{display: "flex", justifyContent: "center"}}>
                  <div className="mockup-code" style={{margin: "20px", width: "100%", maxWidth: "500px"}}>
                    <pre data-prefix=">"><code>imp &quot;@packi/lib.it&quot;;</code></pre>
                    <pre data-prefix=">"><code>packi::Require(&quot;{params.package}&quot;, &quot;{params.latest_version}&quot;);</code></pre>
                  </div>
                </div>
                <p style={{textAlign: "center"}}>
                  Install with <Link href="/packiplus">Packi+</Link>:
                </p>
                <div style={{display: "flex", justifyContent: "center"}}>
                  <div className="mockup-code" style={{margin: "20px", width: "100%", maxWidth: "500px"}}>
                    <pre data-prefix="$"><code>interp @packiplus/cli.it -i {params.package}</code></pre>
                  </div>
                </div>
              </div>
            )
          case "readme":
          case null:
            return (
              <div>
                <div className="flex justify-center">
                  <div className="tabs">
                    <a onClick={() => setTab("install")} className="tab tab-bordered">Install</a> 
                    <a className="tab tab-bordered tab-active">Readme</a> 
                    <a onClick={() => setTab("links")} className="tab tab-bordered">Links</a>
                  </div>
                </div>
                <br/>
                <div className="flex justify-center">
                  <div className="md" style={{maxWidth: "700px", width: "100%", padding: "20px"}} dangerouslySetInnerHTML={{__html: params.readme}} />
                </div>
              </div>
            )
          case "links":
            return (
              <div>
                <div className="flex justify-center">
                  <div className="tabs">
                    <a onClick={() => setTab("install")} className="tab tab-bordered">Install</a> 
                    <a onClick={() => setTab("readme")} className="tab tab-bordered">Readme</a> 
                    <a className="tab tab-bordered tab-active">Links</a>
                  </div>
                </div>
                <br/>
                <div className="flex justify-center">
                  <div style={{maxWidth: "700px", width: "100%", padding: "10px"}}>
                    <a href={params.files}>
                    <div className="rounded shadow bg-base-200" style={{padding: "20px", margin: "20px"}}>
                      <span>Files</span>
                      <span style={{float: "right"}}>

                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-download" viewBox="0 0 16 16">
  <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
  <path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
</svg>             
                    </span>
                    </div>
                    </a>
                    {params.repo == null ? null : <a href={params.repo}>
                    <div className="rounded shadow bg-base-200" style={{padding: "20px", margin: "20px"}}>
                      <span>Repository</span>
                      <span style={{float: "right"}}>

                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-git" viewBox="0 0 16 16">
  <path d="M15.698 7.287 8.712.302a1.03 1.03 0 0 0-1.457 0l-1.45 1.45 1.84 1.84a1.223 1.223 0 0 1 1.55 1.56l1.773 1.774a1.224 1.224 0 0 1 1.267 2.025 1.226 1.226 0 0 1-2.002-1.334L8.58 5.963v4.353a1.226 1.226 0 1 1-1.008-.036V5.887a1.226 1.226 0 0 1-.666-1.608L5.093 2.465l-4.79 4.79a1.03 1.03 0 0 0 0 1.457l6.986 6.986a1.03 1.03 0 0 0 1.457 0l6.953-6.953a1.031 1.031 0 0 0 0-1.457"/>
</svg>
                    </span>
                    </div>
                    </a>}
                    {params.doc == null ? null : <a href={params.doc}>
                    <div className="rounded shadow bg-base-200" style={{padding: "20px", margin: "20px"}}>
                      <span>Documentation</span>
                      <span style={{float: "right"}}>

                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-filetype-doc" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M14 4.5V14a2 2 0 0 1-2 2v-1a1 1 0 0 0 1-1V4.5h-2A1.5 1.5 0 0 1 9.5 3V1H4a1 1 0 0 0-1 1v9H2V2a2 2 0 0 1 2-2h5.5L14 4.5Zm-7.839 9.166v.522c0 .256-.039.47-.117.641a.861.861 0 0 1-.322.387.877.877 0 0 1-.469.126.883.883 0 0 1-.471-.126.868.868 0 0 1-.32-.386 1.55 1.55 0 0 1-.117-.642v-.522c0-.257.04-.471.117-.641a.868.868 0 0 1 .32-.387.868.868 0 0 1 .471-.129c.176 0 .332.043.469.13a.861.861 0 0 1 .322.386c.078.17.117.384.117.641Zm.803.519v-.513c0-.377-.068-.7-.205-.972a1.46 1.46 0 0 0-.589-.63c-.254-.147-.56-.22-.917-.22-.355 0-.662.073-.92.22a1.441 1.441 0 0 0-.589.627c-.136.271-.205.596-.205.975v.513c0 .375.069.7.205.973.137.271.333.48.59.627.257.144.564.216.92.216.357 0 .662-.072.916-.216.256-.147.452-.356.59-.627.136-.274.204-.598.204-.973ZM0 11.926v4h1.459c.402 0 .735-.08.999-.238a1.45 1.45 0 0 0 .595-.689c.13-.3.196-.662.196-1.084 0-.42-.065-.778-.196-1.075a1.426 1.426 0 0 0-.59-.68c-.263-.156-.598-.234-1.004-.234H0Zm.791.645h.563c.248 0 .45.05.609.152a.89.89 0 0 1 .354.454c.079.201.118.452.118.753a2.3 2.3 0 0 1-.068.592 1.141 1.141 0 0 1-.196.422.8.8 0 0 1-.334.252 1.298 1.298 0 0 1-.483.082H.79V12.57Zm7.422.483a1.732 1.732 0 0 0-.103.633v.495c0 .246.034.455.103.627a.834.834 0 0 0 .298.393.845.845 0 0 0 .478.131.868.868 0 0 0 .401-.088.699.699 0 0 0 .273-.248.8.8 0 0 0 .117-.364h.765v.076a1.268 1.268 0 0 1-.226.674c-.137.194-.32.345-.55.454a1.81 1.81 0 0 1-.786.164c-.36 0-.664-.072-.914-.216a1.424 1.424 0 0 1-.571-.627c-.13-.272-.194-.597-.194-.976v-.498c0-.379.066-.705.197-.978.13-.274.321-.485.571-.633.252-.149.556-.223.911-.223.219 0 .421.032.607.097.187.062.35.153.489.272a1.326 1.326 0 0 1 .466.964v.073H9.78a.85.85 0 0 0-.12-.38.7.7 0 0 0-.273-.261.802.802 0 0 0-.398-.097.814.814 0 0 0-.475.138.868.868 0 0 0-.301.398Z"/>
</svg>
                    </span>
                    </div>
                    </a>}
                  </div>
                </div>
              </div>
            )
        }
      })()}

    </div>
  )
}
