import axios from "axios"
import PackageSearcher from "./packageSearcher";

export default async function PackageSearch() {
  
  const fetch: string = (await axios("http://" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY + ":" + process.env.NEXT_PUBLIC_PACKAGE_REGISTRY_PORT + "/package_index.txt")).data;
  const fetch_splitted = fetch.split("\n\n");

  var packages = [];

  for(var i = 0; i < fetch_splitted.length - 1; i++) {
    var fetch_splitted_splitted = fetch_splitted[i].split("\n");
    packages.push({
      id: fetch_splitted_splitted[0],
      version: fetch_splitted_splitted[1],
      summary: fetch_splitted_splitted[2],
    })
  }


    /*packages.push({
      id: id,
      version: latest_version,
      summary: summary,
    })*/

  return (
    <PackageSearcher packages={packages}/>
  )
}