'use client'

import Link from "next/link";
import { useState } from "react"

export default function PackageSearcher({packages}: {packages: Array<any>}) {

  var [searchTerm, setSearchTerm] = useState("");

  packages = packages.filter(a => searchTerm != "" && (a.id.toLowerCase().includes(searchTerm.toLowerCase()) || a.summary.toLowerCase().includes(searchTerm.toLowerCase()) || searchTerm == "*"));

  return (
    <div style={{width: "100%", maxWidth: "700px"}}>
      <input type="text" placeholder="Search Packages" className="input input-bordered w-full" onChange={e => setSearchTerm(e.target.value)}/>
      {packages.map(a => (
    <div key={a.title} style={{width: "100%", marginTop: "35px"}} className="anim">
<div className="card bg-base-200" style={{maxWidth: "700px"}}>
  <div className="card-body items-center text-center">
    <h2 className="card-title">{a.id} - {a.version}</h2>
    <p>{a.summary}</p>
    <div className="card-actions justify-end" style={{marginTop: "20px"}}>
      <Link href={a.id}>Read More</Link>
    </div>
  </div>
</div>
        </div>
  ))}
    </div>
  )
}