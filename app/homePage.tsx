'use client'

import Image from 'next/image'
import { useState } from 'react';
import axios from 'axios';
import PackageSearch from './packageSearch';

export default function Home({children}: any) {

    const [installTab, setInstallTab] = useState(0);
    const [searchTerm, setSearchTerm] = useState("");
  
    return (
      <div style={{height: "100%", display: "flex"}}>
        <div style={{margin: "auto", padding: "10px", width: "100%"}}>
          <h1 style={{fontWeight: "bold", fontSize: "3em", textAlign: "center"}}>Packi</h1>
          <h2 style={{fontWeight: "bold", fontSize: "1.5em", textAlign: "center"}}>The Interp Package Repository</h2>
          <br/>
          <div style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
          <div tabIndex={0} className="collapse collapse-arrow border border-base-300 bg-base-200" style={{maxWidth: "700px"}}>
            <div className="collapse-title text-xl font-medium">
              Install
            </div>
            {(()=>{
              switch(installTab) {
                case 0:
                  return (
                    <div className="collapse-content">
                      <div className="tabs tabs-boxed">
                        <a className="tab tab-active">Linux</a> 
                        <a className="tab" onClick={() => setInstallTab(1)}>Windows</a> 
                        <a className="tab" onClick={() => setInstallTab(2)}>MacOS</a>
                      </div>
                      <div className="mockup-code install-code-box">
                        <pre data-prefix="$"><code>curl https://gitlab.com/interplang/packi-script/-/raw/main/install.it &gt; /tmp/install.it </code></pre>
                        <pre data-prefix="$"><code>interp /tmp/install.it</code></pre>
                      </div>
                    </div>
                  ) 
              case 1:
                  return (
                    <div className="collapse-content">
                      <div className="tabs tabs-boxed">
                        <a className="tab" onClick={() => setInstallTab(0)}>Linux</a> 
                        <a className="tab tab-active" >Windows</a> 
                        <a className="tab" onClick={() => setInstallTab(2)}>MacOS</a>
                      </div>
                      <div className="mockup-code install-code-box">
                        <pre data-prefix="$"><code>curl https://gitlab.com/interplang/packi-script/-/raw/main/install.it &gt; %tmp%/install.it </code></pre>
                        <pre data-prefix="$"><code>interp %tmp%/install.it</code></pre>
                      </div>
                    </div>
                  )
              case 2:
                  return (
                    <div className="collapse-content">
                      <div className="tabs tabs-boxed">
                        <a className="tab" onClick={() => setInstallTab(0)}>Linux</a> 
                        <a className="tab" onClick={() => setInstallTab(1)}>Windows</a> 
                        <a className="tab tab-active">MacOS</a>
                      </div>
                      <div className="mockup-code install-code-box">
                        <pre data-prefix="$"><code>curl https://gitlab.com/interplang/packi-script/-/raw/main/install.it &gt; /tmp/install.it </code></pre>
                        <pre data-prefix="$"><code>interp /tmp/install.it</code></pre>
                      </div>
                    </div>
                  ) 
                }
              })()}
            </div>
          </div>
          <br/>
          <div style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
            {children}
          </div>
          <div className="italic text-center" style={{margin: "50px"}}>
            © <a href="https://kriikkula.com/">Samuel Kriikkula</a>
            <br/>
            This site is a part of the <a href="https://gitlab.com/interplang">interplang project</a>
          </div>
        </div>
      </div>
    )
  }
  
