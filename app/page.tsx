import { Metadata } from 'next'
import Home from './homePage'
import PackageSearch from './packageSearch'

export const metadata: Metadata = {
  title: "Packi",
  description: "The Interp Package Repository"
}

export const dynamic = "force-dynamic";

export default function Page() {
  return (
    <Home>
      <PackageSearch/>
    </Home>
  )
}
